import os
import shutil
import subprocess

import click
import gitlab

from . import helpers
from . import highlight
from .echo import echo
from .exceptions import ClientError

# Solutions local repository


class Solutions(object):
    def __init__(self, config, ci_config):
        self.config = config

        repo_dir = config.data["solutions"]["path"]

        if repo_dir:
            if not os.path.exists(repo_dir):
                raise RuntimeError(
                    "Solutions repository not found at '{}'".format(repo_dir))

        self.repo_dir = repo_dir
        self.ci_config_path = ci_config

    @property
    def attached(self):
        return self.repo_dir is not None

    def _check_attached(self):
        if not self.attached:
            raise RuntimeError("Solutions repository not attached")

    def _git(self, cmd, **kwargs):
        self._check_attached()
        echo.echo("Running git: {}".format(cmd))
        subprocess.check_call(["git"] + cmd, **kwargs)

    def _git_output(self, cmd, **kwargs):
        self._check_attached()
        echo.echo("Running git: {}".format(cmd))
        return subprocess.check_output(["git"] + cmd, **kwargs)

    @staticmethod
    def _task_branch(task):
        return "{}/{}".format(task.homework, task.name)

    def _task_dir(self, task):
        return "{}/{}".format(task.homework, task.name)

    def _create_and_switch_to(self, branch):
        try:
            self._git(["checkout", "-b", branch], cwd=self.repo_dir)
        except subprocess.CalledProcessError:
            self._git(["checkout", branch], cwd=self.repo_dir)

    def _switch_to_branch(self, name):
        self._git(["checkout", name], cwd=self.repo_dir)

    def _switch_to_master(self):
        self._switch_to_branch("master")

    @staticmethod
    def _copy_files(source_dir, dest_dir, files):
        for f in files:
            source_path = os.path.join(source_dir, f)
            if not os.path.exists(source_path):
                raise RuntimeError(
                    "File '{}' not found in '{}'".format(
                        f, source_dir))

        for f in files:
            source_path = os.path.join(source_dir, f)
            shutil.copy(source_path, dest_dir)

    @staticmethod
    def _default_commit_message(task):
        return "Bump task {}/{}".format(task.homework, task.name)

    def commit(self, task, message=None):
        self._check_attached()

        solution_files = task.conf.solution_files

        os.chdir(self.repo_dir)
        echo.echo("Moving to repo {}".format(highlight.path(self.repo_dir)))

        self._switch_to_master()

        task_branch = self._task_branch(task)
        echo.echo("Switching to task branch '{}'".format(task_branch))
        self._create_and_switch_to(task_branch)

        os.chdir(self.repo_dir)

        task_dir = self._task_dir(task)

        if not os.path.exists(task_dir):
            helpers.mkdir(task_dir, parents=True)

        echo.echo("Copy solution files: {}".format(solution_files))
        self._copy_files(task.dir, task_dir, solution_files)

        echo.echo("Adding solution files to index")
        self._git(["add"] + solution_files, cwd=task_dir)

        diff = self._git_output(["diff", "--staged", "."], cwd=task_dir)
        if not diff:
            echo.echo("Empty diff, nothing to commit")
            self._switch_to_master()
            return

        # Add CI config
        if self.ci_config_path:
            shutil.copy(self.ci_config_path, self.repo_dir)
            self._git(["add", ".gitlab-ci.yml"], cwd=self.repo_dir)

        if not message:
            message = self._default_commit_message(task)

        echo.echo("Committing solution files")
        self._git(["commit", "-m", message], cwd=task_dir)

        self._switch_to_master()

    def apply_to(self, task, force=False):
        self._check_attached()

        os.chdir(self.repo_dir)
        echo.echo("Moving to repo {}".format(highlight.path(self.repo_dir)))

        task_branch = self._task_branch(task)
        self._switch_to_branch(task_branch)

        task_dir = self._task_dir(task)

        if not os.path.exists(task_dir):
            raise ClientError(
                "Cannot find task directory '{}' in branch '{}'".format(
                    task_dir, task_branch))

        if force or click.confirm(
                "Apply solutions to task {}?".format(task.fullname)):
            echo.echo("Applying solution from solutions repo...")
            self._copy_files(task_dir, task.dir, task.conf.solution_files)

        self._switch_to_master()

    def push(self, task):
        self._check_attached()

        os.chdir(self.repo_dir)
        echo.echo("Moving to repo {}".format(highlight.path(self.repo_dir)))

        task_branch = self._task_branch(task)

        self._switch_to_branch(task_branch)
        self._git(["push", "origin", task_branch], cwd=self.repo_dir)

        self._switch_to_master()

    def _get_gitlab_repo_address(self):
        url = self.config.get("solutions.url")

        def _cut_dot_git(addr):
            if addr.endswith(".git"):
                addr = addr[:-4]
            return addr

        prefixes = ["https://gitlab.com/", "git@gitlab.com:"]
        for prefix in prefixes:
            if url.startswith(prefix):
                return _cut_dot_git(url[len(prefix):])

        raise ClientError(
            "Cannot get solutions repo address for '{}'".format(url))

    def merge(self, task):
        self._check_attached()

        os.chdir(self.repo_dir)
        echo.echo("Moving to repo {}".format(highlight.path(self.repo_dir)))

        task_branch = self._task_branch(task)
        # Check if task branch exists
        self._switch_to_branch(task_branch)

        echo.echo("Creating merge request...")

        # Create Gitlab client

        token = self.config.get("gitlab.token")
        gitlab_client = gitlab.Gitlab("https://gitlab.com", private_token=token)
        gitlab_client.auth()

        repo_address = self._get_gitlab_repo_address()
        echo.echo("Solutions Gitlab repo: {}".format(repo_address))
        project = gitlab_client.projects.get(repo_address)

        labels = self.config.get("labels")

        merge_request = {
            'source_branch': task_branch,
            'target_branch': 'master',
            'labels': labels,
            'title': 'Homework {}, Task {}'.format(task.homework, task.name),
        }

        try:
            project.mergerequests.create(merge_request)
        except gitlab.exceptions.GitlabCreateError as error:
            if error.response_code == 409:
                echo.echo("Merge request already exists")

        self._switch_to_master()

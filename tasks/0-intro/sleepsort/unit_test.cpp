#include "sleepsort.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <algorithm>

using solutions::SleepSort;

TEST_SUITE(SleepSort) {
  SIMPLE_T_TEST(Empty) {
    std::vector<int> ints;
    SleepSort(ints);
    ASSERT_TRUE(ints.empty());
  }

  SIMPLE_T_TEST(OneTwoThree) {
    std::vector<int> ints{3, 1, 2};
    SleepSort(ints);

    const std::vector<int> kExpected = {1, 2, 3};
    ASSERT_EQ(ints, kExpected);
  }

  SIMPLE_T_TEST(Duplicates) {
    std::vector<int> ints = {1, 2, 3, 4, 5, 4, 3, 2, 1};
    SleepSort(ints);

    const std::vector<int> kExpected = {1, 1, 2, 2, 3, 3, 4, 4, 5};
    ASSERT_EQ(ints, kExpected);
  }

  SIMPLE_T_TEST(MinMax) {
    std::vector<int> ints = {1, 100, 2, 99, 3, 98};
    SleepSort(ints);

    const std::vector<int> kExpected = {1, 2, 3, 98, 99, 100};
    ASSERT_EQ(ints, kExpected);
  }

  SIMPLE_T_TEST(Reversed) {
    const int kNumInts = 100;

    std::vector<int> ints;
    for (int i = 0; i < kNumInts; ++i) {
      ints.push_back(i);
    }
    std::reverse(ints.begin(), ints.end());

    SleepSort(ints);

    ASSERT_EQ(ints.size(), kNumInts);
    for (int i = 0; i < kNumInts; ++i) {
      ASSERT_EQ(ints[i], i);
    }
  }

  SIMPLE_T_TEST(Same) {
    const int kNumInts = 100;
    const int kSameValue = 50;

    std::vector<int> ints;
    for (int i = 0; i < kNumInts; ++i) {
      ints.push_back(kSameValue);
    }

    SleepSort(ints);

    ASSERT_EQ(ints.size(), kNumInts);
    for (int i = 0; i < kNumInts; ++i) {
      ASSERT_EQ(ints[i], kSameValue);
    }
  }

  SIMPLE_T_TEST(NearSame) {
    const int kNumInts = 100;
    const int kSameValue = 50;

    std::vector<int> ints;
    ints.push_back(kSameValue + 1);
    for (int i = 0; i < kNumInts - 1; ++i) {
      ints.push_back(kSameValue);
    }

    SleepSort(ints);

    ASSERT_EQ(ints.size(), kNumInts);
    for (int i = 0; i + 1 < kNumInts; ++i) {
      ASSERT_EQ(ints[i], kSameValue);
    }
    ASSERT_EQ(ints.back(), kSameValue + 1);
  }
}

RUN_ALL_TESTS()

